package com.enkodo.apinews.services

import com.enkodo.apinews.model.NewsResponse
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class NewsApiService {
    companion object {
        val gson = Gson()

        private fun getService(): News {
            val gsonBuilder = GsonBuilder()
                .setLenient()
                .create()

            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.HEADERS
            interceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpClient: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build()

            return Retrofit.Builder()
                .baseUrl("https://newsapi.org/v2/")
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder))
                .build().create(News::class.java)
        }


        fun getNews(country: String, q: String, apiKey: String) =
            CoroutineScope(Dispatchers.IO).async {
                val service = getService()
                return@async gson.fromJson(
                    service.getNews(
                        country,
                        q,
                        apiKey
                    ), NewsResponse::class.java
                )
            }


    }
}

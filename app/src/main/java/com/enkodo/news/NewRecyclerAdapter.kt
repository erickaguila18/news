package com.enkodo.news

import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.enkodo.apinews.model.Article
import com.squareup.picasso.Picasso


class NewRecyclerAdapter(data: List<Article>, var mContext: Context) :
    RecyclerView.Adapter<NewRecyclerAdapter.MyViewHolder?>() {

    var dataSet: List<Article> = data

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView = itemView.findViewById(R.id.title)
        var author: TextView = itemView.findViewById(R.id.author)
        var date: TextView = itemView.findViewById(R.id.date)
        var image: ImageView = itemView.findViewById(R.id.imageview_card)
        var row: LinearLayout = itemView.findViewById(R.id.row)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recyclerview_item_row, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, listPosition: Int) {
        val title = holder.title
        val author = holder.author
        val date = holder.date
        val row = holder.row

        val image: ImageView = holder.image
        title.text = dataSet[listPosition].title
        author.text = dataSet[listPosition].author
        date.text = dataSet[listPosition].publishedAt.subSequence(0, 10)

        row.setOnClickListener {

        }

        Picasso.get().load(dataSet[listPosition].urlToImage).into(image)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

}

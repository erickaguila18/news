package com.enkodo.apinews.services

import com.google.gson.JsonObject
import retrofit2.http.GET
import retrofit2.http.Query


interface News {
    @GET("top-headlines")
    suspend fun getNews(
        @Query("country") country: String,
        @Query("q") q: String?,
        @Query("apiKey") apiKey: String
    ): JsonObject
}
package com.enkodo.news

import android.os.Bundle
import android.text.InputType
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.enkodo.apinews.services.NewsApiService
import com.enkodo.news.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {
    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_main
        )
        mLinearLayoutManager = LinearLayoutManager(this)

        binding.mSwipeRefresh.setOnRefreshListener(this)

        binding.isLoading = true
        binding.mCriteriaEdt.inputType = InputType.TYPE_CLASS_TEXT
        binding.btnGo.setOnClickListener {
            binding.isLoading = false
            callApi()
        }


    }

    override fun onRefresh() {
        callApi()
        binding.mSwipeRefresh.isRefreshing = false
    }


    private fun callApi() {
        GlobalScope.launch(Dispatchers.Main) {
            NewsApiService.getNews(
                binding.mCountryEdt.text.toString(),
                binding.mCriteriaEdt.text.toString(),
                "488b5868bdd043c48adda50cc4ce34e6"
            ).await().let { response ->
                binding.isLoading = true
                binding.mRecyclerView.setHasFixedSize(true)
                binding.mRecyclerView.layoutManager = mLinearLayoutManager
                binding.mRecyclerView.itemAnimator = DefaultItemAnimator()

                binding.mRecyclerView.adapter = NewRecyclerAdapter(
                    response.articles.sortedByDescending { it.publishedAt },
                    applicationContext
                )
            }
        }
    }

}